# README #


### What is this repository for? ###

* VarClust is a R package for clustering Variables
* 0.4

### How do I get set up? ###

* Download the package and install it manually from R command line
* No configuration is needed
* Dependencies install automatically

### Who do I talk to? ###
* standard R manual should be enough to use the package
* If not please contact Piotr.Sobczyk[malpka]pwr.edu.pl